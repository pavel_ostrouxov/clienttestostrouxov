<%@ page import="com.ub.core.language.models.LanguageCode" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form:errors path="*" cssClass="alert alert-warning" element="div" />
<form:hidden path="id"/>

<div class="row">
    <div class="col-lg-6">
        <label for="title">Заголовок</label>
        <form:input path="title" cssClass="form-control" id="title" htmlEscape="true" />
    </div>
    <div class="col-lg-6">
        <label for="publicationDate">Дата публикации</label>
        <form:input path="publicationDate" cssClass="form-control" id="publicationDate"/>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="previewDescription">Пердпросмотр текста</label>
        <textarea name="previewDescription" class="form-control" id="previewDescription">${blogDoc.previewDescription}</textarea>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="content">Описание</label>
        <c:if test="${blogDoc.useCkeditor}">
        <textarea name="content" class="form-control ckeditor" id="content">${blogDoc.content}</textarea>
        </c:if>
        <c:if test="${blogDoc.useCkeditor ne true}">
            <textarea name="content" class="form-control" id="content">${blogDoc.content}</textarea>
        </c:if>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <label for="available">Доступность</label>
        <form:checkbox path="available" class="form-control" id="available"/>
    </div>
    <div class="col-lg-6">
        <label for="useCkeditor">Использовать ckeditor</label>
        <form:checkbox path="useCkeditor" class="form-control" id="useCkeditor"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="tags">Теги</label>
        <input name="tags" id="tags" class="form-control" value="${blogDoc.tags}"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <label for="url">Url</label>
        <input name="url" class="form-control" id="url" value="${blogDoc.url}"/>
    </div>
    <div class="col-lg-4">
        <label for="author">Язык</label>
        <select name="code2" class="form-control" id="lang">
            <c:forEach items="<%= LanguageCode.all()%>" var="langCode">
                <option value="${langCode.code2}" <c:if test="${blogDoc.lang.code2 eq langCode.code2}">selected="true"</c:if>>
                    ${langCode.langNative}
                </option>
            </c:forEach>
        </select>
    </div>
    <div class="col-lg-4">
        <label for="author">Автор</label>
        <input name="author" class="form-control" id="author" value="${blogDoc.author}"/>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <label for="previewPic">Изображение для предпросмотра</label>
        <input name="previewPic" type="file" class="form-control" id="previewPic"/>
        <img src="/pics/${blogDoc.previewPic}" style="width: 10%"/>
    </div>
</div>

