<%@ page import="com.ub.ubblog.blog.routes.BlogAdminRoutes" %>
<%@ page import="com.ub.core.language.services.DifferentLanguagesService" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>
<div class="row">
    <div class="col-lg-12">
        <div class="panel minimal minimal-gray">
            <div class="panel-heading">
                <div class="panel-title"><h4>Редактирование</h4></div>
                <div class="panel-options">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#info" data-toggle="tab">Общая информация</a></li>
                        <li><a href="#seo" data-toggle="tab">Seo</a></li>
                        <c:if test="<%= DifferentLanguagesService.useLanguagePackage%>">
                            <li><a href="#langTab" data-toggle="tab">Языки</a></li>
                        </c:if>
                    </ul>
                </div>
            </div>

            <div class="panel-body">

                <div class="tab-content">
                    <div class="tab-pane active" id="info">
                        <form:form action="<%= BlogAdminRoutes.EDIT%>" modelAttribute="blogDoc" method="POST"
                                   enctype="multipart/form-data">
                            <c:set value="${blogDoc}" var="blogDoc" scope="request"/>
                            <jsp:include page="form.jsp"/>
                            <br>

                            <div class="form-group">
                                <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">
                                    Сохранить
                                </button>
                            </div>

                        </form:form>
                        <div class="row">
                            <div class="col-lg-12">
                                <h3>Добавить файл:</h3>

                                <div class="widget-content">
                                    <form action="<%= BlogAdminRoutes.ADD_FILE%>" method="POST" enctype="multipart/form-data">
                                        <input type="hidden" name="blogId" value="${blogDoc.id}">
                                        <div class="row">
                                            <div class="col-lg-3">
                                                <input type="file" name="file" required>
                                            </div>
                                            <div class="col-lg-3">
                                                <button class="btn btn-success" type="submit">Добавить</button>
                                            </div>
                                        </div>

                                    </form>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <hr/>
                                <h3>Файлы</h3>
                                <table class="table">
                                    <thead></thead>
                                    <tbody>
                                    <c:forEach items="${blogDoc.files.keySet()}" var="fileId">
                                        <tr>
                                            <td style="width:30%"><a href="/files/${fileId}">
                                                <div><h5
                                                        class="breaking">${blogDoc.files.get(fileId)}</h5></div>
                                            </a></td>
                                            <td style="width:25%">
                                                <form action="<%= BlogAdminRoutes.DELETE_FILE%>" method="post">
                                                    <input type="hidden" name="fileId" value="${fileId}">
                                                    <input type="hidden" name="blogId" value="${blogDoc.id}">
                                                    <button type="submit" class="btn btn-danger btn-xs">удалить</button>
                                                </form>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="seo">
                        <form:form action="<%= BlogAdminRoutes.EDIT_SEO%>" modelAttribute="seoTags" method="POST">
                            <input type="hidden" name="id" value="${blogDoc.id}"/>
                            <c:set value="${blogDoc.seoTags}" var="seoTags" scope="request"/>
                            <jsp:include page="/WEB-INF/com/ub/core/admin/seoTags/form.jsp"/>
                            <br>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </form:form>
                    </div>
                    <c:if test="<%= DifferentLanguagesService.useLanguagePackage%>">
                        <div class="tab-pane" id="langTab">
                            Текущий язык документа - ${blogDoc.lang.langNative}
                            <br>
                            Доступные языки:
                            <c:forEach items="${blogLangs}" var="blogLangDoc">
                                <c:url var="urlBlogLang" value="<%= BlogAdminRoutes.EDIT%>">
                                    <c:param name="id" value="${blogLangDoc.id}"/>
                                </c:url>
                                <br><a href="${urlBlogLang}">Редактировать - "${blogLangDoc.title}"
                                - ${blogLangDoc.lang.langNative}</a>
                            </c:forEach>
                            <br>
                            <c:url var="trsnUrl" value="<%= BlogAdminRoutes.ADD%>">
                                <c:param name="parentBlogId" value="${blogDoc.id}"/>
                            </c:url>
                            <a href="${trsnUrl}">Добавить перевод для этого документа</a>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</div>