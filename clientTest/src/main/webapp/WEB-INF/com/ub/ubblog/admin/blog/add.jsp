<%@ page import="com.ub.ubblog.blog.routes.BlogAdminRoutes" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<jsp:include page="/WEB-INF/com/ub/core/admin/main/components/pageHeader.jsp"/>
<div class="row">
    <div class="col-lg-12">
        <div class="widget widget-green" id="widget_profit_chart">
            <div class="widget-title">
                <h3><i class="icon-tasks"></i> Добавить</h3>
            </div>
            <div class="widget-content">

                <form:form action="<%= BlogAdminRoutes.ADD%>" modelAttribute="blogDoc" method="POST"
                           enctype="multipart/form-data">

                    <c:set value="${blogDoc}" var="blogDoc" scope="request"/>
                    <jsp:include page="form.jsp"/>

                    <c:if test="${not empty parentBlogId}">
                        <input type="hidden" name="parentBlogId" value="${parentBlogId}"/>
                    </c:if>
                    <div class="row">
                        <div class="col-lg-12">
                            <label for="file">Добавить файлы:</label>

                            <input type="file" name="files" id="file" multiple>

                        </div>
                    </div>
                    <br>
                    <div class="form-group">
                        <button type="submit" id="contact-form-settings-submit" class="btn btn-primary">Сохранить
                        </button>
                    </div>
                </form:form>

            </div>
        </div>
    </div>
</div>