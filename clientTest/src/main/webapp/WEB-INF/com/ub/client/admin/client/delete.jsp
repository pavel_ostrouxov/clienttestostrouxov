<%@ page import="com.ub.clientTest.client.routes.ClientAdminRoutes" %><%--
  Created by IntelliJ IDEA.
  User: UnitBean User
  Date: 06.09.2018
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="row">
    <div class="col-md-12">
        <h1>Подтвердите удаление информации о клиенте</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <form action="<%= ClientAdminRoutes.DELETE%>" method="post">
            <input type="hidden" name="id" value="${id}">
            <button type="submit" class="btn btn-danger">Да, удалить</button>
        </form>
    </div>
</div>
