<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${not empty clientDoc.pic}">
    <form:hidden path="id"/>
</c:if>


<div class="row">
    <div class="col-lg-12">
        <label for="email">Email</label>
        <form:input path="email" cssClass="form-control" id="email"/>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="name">Имя</label>
        <form:input path="name" cssClass="form-control" id="name"/>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="lastName">Фамилия</label>
        <form:input path="lastName" cssClass="form-control" id="lastName"/>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="pic">Аватар</label>
        <c:if test="${not empty clientDoc.pic}">
            <form:hidden path="pic" cssClass="form-control" id="pic"/>
            <img style="max-width: 100%" src="/pics/${clientDoc.pic}"/>
        </c:if>
        <input name="picFile" type="file" id="picFile">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="fileFile">Карта партнера (<a href="/files/${clientDoc.file}"> Текущая карта</a>)</label>
        <c:if test="${not empty clientDoc.file}">
            <form:hidden path="file" cssClass="form-control" id="file"/>
        </c:if>
        <input name="fileFile" type="file" id="fileFile">
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <label for="typeUser">Тип пользователя</label>
        <form:select path="clientTypeDoc.id" cssClass="form-control" id="typeUser">
            <c:forEach items="${clientTypes}" var="cType">
                <form:option value="${cType}">${cType.title}</form:option>
            </c:forEach>
        </form:select>
    </div>
</div>
