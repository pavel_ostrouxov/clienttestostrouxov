<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientRoutes" %>

<div class="container top-container">


        <div class="row">
            <div class="col-md-12">
                <h1>${blogDoc.title}</h1>
            </div>
        </div>
    <div class="row">
        <div class="col-md-12">
            <p>${blogDoc.content}</p>
        </div>
    </div>

</div>
