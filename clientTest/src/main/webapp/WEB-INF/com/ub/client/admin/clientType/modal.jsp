<%@ page import="com.ub.client.clientType.routes.ClientTypeAdminRoutes" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<div class="modal fade custom-width" id="modal-clientType">
    <div class="modal-dialog" style="width: 96%">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Выбор Типы клиентов</h4>
            </div>

            <div class="modal-body">

                <div class="row">

                    <div class="col-lg-5">
                        <div class="input-group">
                            <input type="text" class="form-control input-sm" id="modal-clientType-query" name="query"
                                   value="" placeholder="Поиск"/>

                            <div class="input-group-btn">
                                <button type="submit" id="modal-clientType-search" class="btn btn-sm btn-default"><i
                                        class="entypo-search">Поиск </i>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
                <section id="modal-clientType-parent-content"></section>

            </div>
        </div>
    </div>
</div>

<script>
    function initClientTypeModal() {
        $.get("<%= ClientTypeAdminRoutes.MODAL_PARENT%>",
                {query: $('#modal-clientType-query').val()},
                function (data) {
                    updateClientTypeContent(data);
                });
    }

    function updateClientTypeContent(data) {
        $('#modal-clientType-parent-content').html(data);
    }

    function onClickClientTypeMTable() {
        var id = $(this).attr('data-id');
        var title = $(this).attr('data-title');
        $('#parent-clientType-hidden').val(id);
        $('#parent-clientType').val(title);
        $('#modal-clientType').modal('hide');
    }

    function onClickClientTypePaginator() {
        var q = $(this).attr('data-query');
        var n = $(this).attr('data-number');
        $.get("<%= ClientTypeAdminRoutes.MODAL_PARENT%>",
                {query: q, currentPage: n},
                function (data) {
                    updateClientTypeContent(data);
                });
    }

    $(function () {
        $('#btn_parent_clientType').click(function () {
            $('#modal-clientType').modal('show');
            initClientTypeModal();
            return false;
        });
        $('#btn_parent_clientType_clear').click(function () {
            $('#parent-clientType-hidden').val('');
            $('#parent-clientType').val('');
            return false;
        });

        $('#modal-clientType').on('click', '.modal-clientType-line', onClickClientTypeMTable);
        $('#modal-clientType').on('click', '.modal-clientType-goto', onClickClientTypePaginator);
        $('#modal-clientType-search').click(initClientTypeModal);

    });
</script>
