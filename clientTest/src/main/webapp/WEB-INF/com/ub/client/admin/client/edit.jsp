<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientAdminRoutes" %><%--
  Created by IntelliJ IDEA.
  User: UnitBean User
  Date: 06.09.2018
  Time: 13:44
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="col-md-12"><h1>Редактирование</h1></div>
</div>
<div class="row">
    <div class="col-md-12">
        <form:form action="<%= ClientAdminRoutes.EDIT%>" method="post" modelAttribute="clientDoc" enctype="multipart/form-data">

            <%--<c:set var="clientDoc" value="${clientDoc}" scope="requeat"/>--%>

            <jsp:include page="form.jsp"/>

            <br>
            <button class="btn btn-success" type="submit">Сохранить</button>
        </form:form>
    </div>
</div>
