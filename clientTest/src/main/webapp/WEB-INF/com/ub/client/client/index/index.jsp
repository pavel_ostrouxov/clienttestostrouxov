<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientRoutes" %>
<div class="container top-container">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Добрый день</h1>
            <h3>Заполните форму подписки</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <c:if test="${success eq true}">
                <div class="alert alert-success">Вы успешно подписаны</div>
            </c:if>

            <c:if test="${error eq true}">
                <div class="alert alert-danger">Вы уже подписаны</div>
            </c:if>


            <h4>Форма подписки</h4>
            <form action="<%= ClientRoutes.ROOT%>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="input-email">Email</label>
                    <input name="email" type="email" class="form-control" id="input-email" required
                           placeholder="Введите email">
                </div>
                <div class="form-group">
                    <label for="input-name">Имя</label>
                    <input name="name" type="text" class="form-control" id="input-name" required
                           placeholder="Введите Ваше имя">
                </div>
                <div class="form-group">
                    <label for="input-lastName">Фамилия</label>
                    <input name="lastName" type="text" class="form-control" id="input-lastName"
                           placeholder="Введите Вашу фамилию">
                </div>
                <div class="form-group">
                    <label for="input-pic">Аватар</label>
                    <input name="pic" type="file" id="input-pic">
                </div>

                <div class="form-group">
                    <label for="input-file">Карта партнера</label>
                    <input name="file" type="file" id="input-file">
                </div>

                <div class="form-group">
                    <label for="input-pic-type-user">Обращение</label>
                    <select name="typeUser" id="input-pic-type-user" class="form-control">
                        <c:forEach items="${clientTypes}" var="cType">
                            <option value="${cType.id}">${cType.title}</option>
                        </c:forEach>
                    </select>
                </div>

                <button type="submit" class="btn btn-default">Оформить подписку</button>
            </form>
        </div>
    </div>
</div>