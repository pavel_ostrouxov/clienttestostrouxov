<%@ page import="com.ub.clientTest.client.routes.ClientAdminRoutes" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <div class="col-md-12">Результаты поиска</div>
</div>

<div class="widget widget-blue">
    <div class="widget-title">
        <h3>Результаты поиска:</h3>
    </div>
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>email</th>
                        <th>ФИО</th>
                        <th>Тип пользователя</th>
                        <th>Действия</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${searchClientAdminResponse.result}" var="clientDoc">
                        <tr>
                            <td>${clientDoc.email}</td>
                            <td>${clientDoc.name} ${clientDoc.lastName}</td>
                            <td>${clientDoc.clientTypeDoc.title}</td>
                            <td>
                                <c:url value="<%= ClientAdminRoutes.EDIT%>" var="urlEdit">
                                    <c:param name="id" value="${clientDoc.id}" />
                                </c:url>
                                <a href="${urlEdit}" class="btn btn-default btn-xs">Редактирование</a>

                                <c:url value="<%= ClientAdminRoutes.DELETE%>" var="urlDelete">
                                    <c:param name="id" value="${clientDoc.id}" />
                                </c:url>
                                <a href="${urlDelete}" class="btn btn-danger btn-xs">Удаление</a>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 text-center">
        <ul class="pagination pagination-sm">
            <c:url value="<%= ClientAdminRoutes.ALL%>" var="urlPrev">
                <c:param name="currentPage" value="${searchClientAdminResponse.prevNum()}"/>
            </c:url>
            <li><a href="${urlPrev}"><i class="entypo-left-open-mini"></i> </a></li>

            <c:forEach items="${searchClientAdminResponse.paginator()}" var="page">
                <c:url value="<%=ClientAdminRoutes.ALL%>" var="url">
                    <c:param name="currentPage" value="${page}"/>
                </c:url>
                <li>
                    <a href="${url}"
                       class="<c:if test="${searchClientAdminResponse.currentPage eq page}">active</c:if> ">
                            ${page + 1}
                    </a>
                </li>
            </c:forEach>

            <c:url value="<%= ClientAdminRoutes.ALL%>" var="urlNext">
                <c:param name="currentPage" value="${searchClientAdminResponse.nextNum()}"/>
            </c:url>
            <li><a href="${urlNext}"><i class="entypo-right-open-mini"></i> </a></li>
        </ul>
    </div>
</div>