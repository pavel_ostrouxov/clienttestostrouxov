<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="com.ub.clientTest.client.routes.ClientRoutes" %>

<div class="container top-container">

    <c:forEach items="${blogs}" var="blogDoc">
        <div class="row">
            <div class="col-md-12">
                <h3><a href="/blog/${blogDoc.url}">${blogDoc.title}</a></h3>
                <p>${blogDoc.previewDescription}</p>
            </div>
        </div>
    </c:forEach>


</div>
