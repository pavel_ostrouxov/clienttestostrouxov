    <%@ page import="com.ub.core.base.routes.BaseRoutes" %>
        <%@ page import="com.ub.clientTest.client.routes.ClientRoutes" %>
        <%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Добро пожаловать!</title>
    <link rel="stylesheet" href="/static/client/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/static/client/css/custom.css"/>
</head>
<body>
    <div class="container">
        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a href="<%= ClientRoutes.ROOT%>" class="navbar-brand">UnitBean</a>
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                        <li>
                            <a href="<%= ClientRoutes.ROOT%>">Главная</a>
                        </li>
                        <li>
                            <a href="<%= BaseRoutes.ADMIN%>">Админ. панель</a>
                        </li>
                        <li>
                        <a href="<%= ClientRoutes.BLOG_ALL%>">Блог</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="row">
            <div class="col-md-12 text-center">
                Hello
            </div>
        </div>
    </div>
    <tiles:insertAttribute name="content" />
</body>
</html>
