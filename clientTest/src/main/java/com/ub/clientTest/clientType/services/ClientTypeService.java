package com.ub.client.clientType.services;

import com.ub.client.clientType.models.ClientTypeDoc;
import com.ub.client.clientType.events.IClientTypeEvent;
import com.ub.client.clientType.views.all.SearchClientTypeAdminRequest;
import com.ub.client.clientType.views.all.SearchClientTypeAdminResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

@Component
public class ClientTypeService {
    private static Map<String, IClientTypeEvent> events = new HashMap<String, IClientTypeEvent>();

    @Autowired private MongoTemplate mongoTemplate;

    public static void addEvent(IClientTypeEvent event) {
        events.put(event.getClass().getCanonicalName(), event);
    }

    public ClientTypeDoc save(ClientTypeDoc doc) {
        doc.setUpdateAt(new Date());
        mongoTemplate.save(doc);
        callAfterSave(doc);
        return doc;
    }

    public ClientTypeDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, ClientTypeDoc.class);
    }

    public void remove(ObjectId id) {
        ClientTypeDoc doc = findById(id);
        if (doc == null) return;
        mongoTemplate.remove(doc);
        callAfterDelete(doc);
    }

    public List<ClientTypeDoc> findAll() {
        return mongoTemplate.findAll(ClientTypeDoc.class);
    }

    public Long count(Query query) {
        return mongoTemplate.count(query, ClientTypeDoc.class);
    }

    public SearchClientTypeAdminResponse findAll(SearchClientTypeAdminRequest request) {
        Sort sort = new Sort(Sort.Direction.DESC, "id");
        Pageable pageable = new PageRequest(
                request.getCurrentPage(),
                request.getPageSize(),
                sort);

        Criteria criteria = new Criteria();
        criteria = Criteria.where("title").regex(request.getQuery(), "i");

        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, ClientTypeDoc.class);
        query = query.with(pageable);

        List<ClientTypeDoc> result = mongoTemplate.find(query, ClientTypeDoc.class);
        SearchClientTypeAdminResponse response = new SearchClientTypeAdminResponse(
                request.getCurrentPage(),
                request.getPageSize(),
                result);
        response.setAll(count);
        response.setQuery(request.getQuery());
        return response;
    }

    private void callAfterSave(ClientTypeDoc doc) {
        for (IClientTypeEvent event : events.values()) {
            event.afterSave(doc);
        }
    }

    private void callAfterDelete(ClientTypeDoc doc) {
        for (IClientTypeEvent event : events.values()) {
            event.afterDelete(doc);
        }
    }
}
