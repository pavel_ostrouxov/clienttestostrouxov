package com.ub.client.clientType.events;

import com.ub.client.clientType.models.ClientTypeDoc;

public interface IClientTypeEvent {
    public void preSave(ClientTypeDoc doc);
    public void afterSave(ClientTypeDoc doc);

    public Boolean preDelete(ClientTypeDoc doc);
    public void afterDelete(ClientTypeDoc doc);
}
