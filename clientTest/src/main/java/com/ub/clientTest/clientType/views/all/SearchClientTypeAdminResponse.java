package com.ub.client.clientType.views.all;

import com.ub.core.base.search.SearchResponse;
import com.ub.client.clientType.models.ClientTypeDoc;

import java.util.List;

public class SearchClientTypeAdminResponse extends SearchResponse {
    private List<ClientTypeDoc> result;


    public SearchClientTypeAdminResponse() {
    }

    public SearchClientTypeAdminResponse(Integer currentPage, Integer pageSize, List<ClientTypeDoc> result) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
        this.result = result;
    }

    public List<ClientTypeDoc> getResult() {
        return result;
    }

    public void setResult(List<ClientTypeDoc> result) {
        this.result = result;
    }
}
