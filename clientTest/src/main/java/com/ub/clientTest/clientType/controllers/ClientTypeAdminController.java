package com.ub.client.clientType.controllers;

import com.mongodb.gridfs.GridFSDBFile;
import com.ub.client.clientType.models.ClientTypeDoc;
import com.ub.client.clientType.routes.ClientTypeAdminRoutes;
import com.ub.client.clientType.services.ClientTypeService;
import com.ub.client.clientType.views.all.SearchClientTypeAdminRequest;
import com.ub.client.clientType.views.all.SearchClientTypeAdminResponse;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.file.services.FileService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ub.core.base.views.breadcrumbs.BreadcrumbsLink;
import com.ub.core.base.views.pageHeader.PageHeader;

@Controller
public class ClientTypeAdminController {

    @Autowired private ClientTypeService clientTypeService;

    private PageHeader defaultPageHeader(String current) {
        PageHeader pageHeader = PageHeader.defaultPageHeader();
        pageHeader.setLinkAdd(ClientTypeAdminRoutes.ADD);
        pageHeader.getBreadcrumbs().getLinks().add(new BreadcrumbsLink(ClientTypeAdminRoutes.ALL, "Все Типы клиентов"));
        pageHeader.getBreadcrumbs().setCurrentPageTitle(current);
        return pageHeader;
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ADD, method = RequestMethod.GET)
    public String create(Model model) {
        ClientTypeDoc doc = new ClientTypeDoc();
        doc.setId(new ObjectId());
        model.addAttribute("doc", doc);
        model.addAttribute("pageHeader", defaultPageHeader("Добавление"));
        return "com.ub.client.admin.clientType.add";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ADD, method = RequestMethod.POST)
    public String create(@ModelAttribute("doc") ClientTypeDoc doc,
                         RedirectAttributes redirectAttributes) {
        clientTypeService.save(doc);
        redirectAttributes.addAttribute("id", doc.getId());
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientTypeAdminRoutes.EDIT, method = RequestMethod.GET)
    public String update(@RequestParam ObjectId id, Model model) {
        ClientTypeDoc doc = clientTypeService.findById(id);
        model.addAttribute("doc", doc);
        model.addAttribute("pageHeader", defaultPageHeader("Редактирование"));
        return "com.ub.client.admin.clientType.edit";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.EDIT, method = RequestMethod.POST)
    public String update(@ModelAttribute("doc") ClientTypeDoc doc,
                         RedirectAttributes redirectAttributes) {
        clientTypeService.save(doc);
        redirectAttributes.addAttribute("id", doc.getId());
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientTypeAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      @RequestParam(required = false, defaultValue = "") String query,
                      Model model) {
        SearchClientTypeAdminRequest searchRequest = new SearchClientTypeAdminRequest(currentPage);
        searchRequest.setQuery(query);
        model.addAttribute("search", clientTypeService.findAll(searchRequest));
        model.addAttribute("pageHeader", defaultPageHeader("Все"));
        return "com.ub.client.admin.clientType.all";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.MODAL_PARENT, method = RequestMethod.GET)
    public String modalResponse(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                @RequestParam(required = false, defaultValue = "") String query,
                                Model model) {
        SearchClientTypeAdminRequest searchRequest = new SearchClientTypeAdminRequest(currentPage);
        searchRequest.setQuery(query);
        model.addAttribute("search", clientTypeService.findAll(searchRequest));
        return "com.ub.client.admin.clientType.modal.parent";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("pageHeader", defaultPageHeader("Удаление"));
        return "com.ub.client.admin.clientType.delete";
    }

    @RequestMapping(value = ClientTypeAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        clientTypeService.remove(id);
        return RouteUtils.redirectTo(ClientTypeAdminRoutes.ALL);
    }
}
