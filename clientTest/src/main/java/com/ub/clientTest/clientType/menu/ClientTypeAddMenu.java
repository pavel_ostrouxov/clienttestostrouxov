package com.ub.client.clientType.menu;

import com.ub.client.clientType.routes.ClientTypeAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class ClientTypeAddMenu extends CoreMenu {
    public ClientTypeAddMenu() {
        this.name = "Добавить";
        this.parent = new ClientTypeMenu();
        this.url = ClientTypeAdminRoutes.ADD;
    }
}
