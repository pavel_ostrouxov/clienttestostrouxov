package com.ub.client.clientType.menu;

import com.ub.core.base.menu.CoreMenu;
import com.ub.core.menu.models.fields.MenuIcons;

public class ClientTypeMenu extends CoreMenu {
    public ClientTypeMenu() {
        this.name = "Типы клиентов";
        this.icon = MenuIcons.ENTYPO_DOC_TEXT;
    }
}
