package com.ub.client.clientType.menu;

import com.ub.client.clientType.routes.ClientTypeAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class ClientTypeAllMenu extends CoreMenu {
    public ClientTypeAllMenu() {
        this.name = "Все";
        this.parent = new ClientTypeMenu();
        this.url = ClientTypeAdminRoutes.ALL;
    }
}
