package com.ub.client.clientType.views.all;

import com.ub.core.base.search.SearchRequest;

public class SearchClientTypeAdminRequest extends SearchRequest {
    public SearchClientTypeAdminRequest() {
    }

    public SearchClientTypeAdminRequest(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public SearchClientTypeAdminRequest(Integer currentPage, Integer pageSize) {
        this.pageSize = pageSize;
        this.currentPage = currentPage;
    }

}
