package com.ub.clientTest.client.menu;

import com.ub.clientTest.client.routes.ClientAdminRoutes;
import com.ub.core.base.menu.CoreMenu;

public class ClientAllMenu extends CoreMenu {
    public ClientAllMenu() {
        this.name = "Все";
        this.url = ClientAdminRoutes.ALL;
        this.parent = new ClientMenu();
    }
}
