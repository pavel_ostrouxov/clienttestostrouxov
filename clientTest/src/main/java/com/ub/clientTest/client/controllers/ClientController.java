package com.ub.clientTest.client.controllers;

import com.ub.client.clientType.services.ClientTypeService;
import com.ub.clientTest.client.routes.ClientRoutes;
import com.ub.clientTest.client.services.ClientService;
import com.ub.clientTest.client.services.exception.ClientExistException;
import com.ub.core.base.httpResponse.ResourceNotFoundException;
import com.ub.ubblog.blog.models.BlogDoc;
import com.ub.ubblog.blog.services.BlogService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ClientController {

    @Autowired private ClientService clientService;
    @Autowired private ClientTypeService clientTypeService;
    @Autowired private BlogService blogService;

    @RequestMapping(value = ClientRoutes.ROOT, method = RequestMethod.GET)
    public String index(Model model){
        model.addAttribute("clientTypes", clientTypeService.findAll());
        return "com.ub.client.index";
    }

    @RequestMapping(value = ClientRoutes.ROOT, method = RequestMethod.POST)
    public String index(@RequestParam String email, @RequestParam String name,
                        @RequestParam(required = false, defaultValue = "") String lastName,
                        @RequestParam(required = false) MultipartFile pic,
                        @RequestParam(required = false) MultipartFile file,
                        @RequestParam ObjectId typeUser, Model model){

        try {
            clientService.create(email, name, lastName, pic, file, typeUser);
            model.addAttribute("success", true);
        } catch (ClientExistException e) {
            model.addAttribute("error", true);
        }
        model.addAttribute("clientTypes", clientTypeService.findAll());
        return "com.ub.client.index";
    }

    @RequestMapping(value = ClientRoutes.BLOG_ALL, method = RequestMethod.GET)
    public String blogAll(Model model) {
        model.addAttribute("blogs", blogService.findActiveAll(10));
        return "com.ub.client.blog.all";
    }

    @RequestMapping(value = ClientRoutes.BLOG_VIEW, method = RequestMethod.GET)
    public String blogView(@PathVariable String url, Model model) {
        BlogDoc blogDoc = blogService.findByUrlAvailable(url);
        if(blogDoc == null) {
            throw new ResourceNotFoundException();
        }

        model.addAttribute("blogDoc", blogDoc);

        return "com.ub.client.blog.view";
    }
}
