package com.ub.clientTest.client.routes;

public class ClientRoutes {

    public static final String ROOT = "/";

    public static final String BLOG_ALL="/blog/all";
    public static final String BLOG_VIEW="/blog/{url}";
}
