package com.ub.clientTest.client.services;

import com.ub.clientTest.client.modules.ClientDoc;
import com.ub.clientTest.client.services.exception.ClientExistException;
import com.ub.clientTest.client.view.SearchClientAdminRequest;
import com.ub.clientTest.client.view.SearchClientAdminResponse;
import com.ub.core.file.services.FileService;
import com.ub.core.picture.services.PictureService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import com.ub.client.clientType.services.ClientTypeService;
import com.ub.client.clientType.models.ClientTypeDoc;

import java.util.List;

@Component
public class ClientService {
    @Autowired private MongoTemplate mongoTemplate;
    @Autowired private PictureService pictureService;
    @Autowired private FileService fileService;
    @Autowired private ClientTypeService clientTypeService;

    public ClientDoc create(String email, String name,
                            String lastName, MultipartFile pic, MultipartFile file, ObjectId clientTypeId) throws ClientExistException {
        ClientDoc clientDoc = findByEmail(email);
        if(clientDoc != null) throw new ClientExistException();
        clientDoc = new ClientDoc();
        clientDoc.setEmail(email);
        clientDoc.setName(name);
        clientDoc.setLastName(lastName);

        ClientTypeDoc clientTypeDoc = clientTypeService.findById(clientTypeId);
        clientDoc.setClientTypeDoc(clientTypeDoc);

        ObjectId picId = pictureService.saveWithDelete(pic, null);
        ObjectId fileId = fileService.saveWithDelete(file, null);

        clientDoc.setPic(picId);
        clientDoc.setFile(fileId);

        return save(clientDoc);
    }

    public ClientDoc findByEmail(String email) {
        Criteria criteria = new Criteria().where("email").is(email);
        return mongoTemplate.findOne(new Query(criteria), ClientDoc.class);
    }

    public ClientDoc findById(ObjectId id) {
        return mongoTemplate.findById(id, ClientDoc.class);
    }

    public ClientDoc save(ClientDoc clientDoc){
        if(clientDoc.getClientTypeDoc() != null) {
            ClientTypeDoc clientTypeDoc = clientTypeService.findById(clientDoc.getClientTypeDoc().getId());
            clientDoc.setClientTypeDoc(clientTypeDoc);
        }

        mongoTemplate.save(clientDoc);
        return clientDoc;
    }

    public void delete(ObjectId id) {
        ClientDoc clientDoc = findById(id);
        mongoTemplate.remove(clientDoc);
    }

    public SearchClientAdminResponse findAll(SearchClientAdminRequest searchClientAdminRequest) {
        Sort sort = new Sort(Sort.Direction.ASC, "email");
        searchClientAdminRequest.setPageSize(10);
        Pageable pageable = new PageRequest(
                searchClientAdminRequest.getCurrentPage(),
                searchClientAdminRequest.getPageSize(),
                sort
        );

        Criteria criteria = new Criteria();
        Query query = new Query(criteria);
        Long count = mongoTemplate.count(query, ClientDoc.class);
        query = query.with(pageable);

        List<ClientDoc> result = mongoTemplate.find(query, ClientDoc.class);
        SearchClientAdminResponse response = new SearchClientAdminResponse(
                searchClientAdminRequest.getCurrentPage(),
                searchClientAdminRequest.getPageSize(),
                result);

        response.setAll(count);
        return response;
    }
}
