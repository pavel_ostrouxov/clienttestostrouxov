package com.ub.clientTest.client.view;

import com.ub.core.base.search.SearchRequest;

public class SearchClientAdminRequest extends SearchRequest {
    public SearchClientAdminRequest(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public SearchClientAdminRequest() {
    }
}
