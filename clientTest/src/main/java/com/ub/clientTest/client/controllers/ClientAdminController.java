package com.ub.clientTest.client.controllers;

import com.ub.clientTest.client.modules.ClientDoc;
import com.ub.clientTest.client.routes.ClientAdminRoutes;
import com.ub.clientTest.client.routes.ClientRoutes;
import com.ub.clientTest.client.services.ClientService;
import com.ub.clientTest.client.services.exception.ClientExistException;
import com.ub.clientTest.client.view.SearchClientAdminRequest;
import com.ub.clientTest.client.view.SearchClientAdminResponse;
import com.ub.core.base.utils.RouteUtils;
import com.ub.core.file.services.FileService;
import com.ub.core.picture.services.PictureService;
import com.ub.core.security.annotationSecurity.AvailableForRoles;
import com.ub.client.clientType.services.ClientTypeService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ClientAdminController {
    @Autowired private ClientService clientService;
    @Autowired private PictureService pictureService;
    @Autowired private FileService fileService;
    @Autowired private ClientTypeService clientTypeService;

    @RequestMapping(value = ClientAdminRoutes.ADD, method = RequestMethod.GET)
    public String add(Model model) {
        ClientDoc clientDoc = new ClientDoc();
        model.addAttribute("clientDoc", clientDoc);
        model.addAttribute("clientTypes", clientTypeService.findAll());
        return "com.ub.clientTest.admin.add";
    }

    @RequestMapping(value = ClientAdminRoutes.ADD, method = RequestMethod.POST)
    public String add(@ModelAttribute ClientDoc clientDoc,
                      @RequestParam(required = false) MultipartFile picFile,
                      @RequestParam(required = false) MultipartFile fileFile,
                      RedirectAttributes re) {
        try {
            clientDoc = clientService.create(clientDoc.getEmail(), clientDoc.getName(),
                    clientDoc.getLastName(), picFile, fileFile, clientDoc.getClientTypeDoc().getId());
        } catch (ClientExistException e) {
            ClientDoc tmp = clientService.findByEmail(clientDoc.getEmail());
            re.addAttribute("id", tmp.getId());
            return RouteUtils.redirectTo(ClientAdminRoutes.EDIT);
        }

        re.addAttribute("id", clientDoc.getId());
        return RouteUtils.redirectTo(ClientAdminRoutes.EDIT);
    }

    @RequestMapping(value = ClientAdminRoutes.EDIT, method = RequestMethod.GET)
    public String edit(@RequestParam ObjectId id, Model model) {
        ClientDoc clientDoc = clientService.findById(id);
        model.addAttribute("id", id);
        model.addAttribute("clientDoc", clientDoc);
        model.addAttribute("clientTypes", clientTypeService.findAll());
        return "com.ub.clientTest.admin.edit";
    }

    @RequestMapping(value = ClientAdminRoutes.EDIT, method = RequestMethod.POST)
    public String edit(@ModelAttribute ClientDoc clientDoc,
                       @RequestParam(required = false) MultipartFile picFile,
                       @RequestParam(required = false) MultipartFile fileFile,
                       RedirectAttributes re) {
        ObjectId picId = pictureService.saveWithDelete(picFile, clientDoc.getPic());
        clientDoc.setPic(picId);
        ObjectId fileId = fileService.saveWithDelete(fileFile, clientDoc.getFile());
        clientDoc.setFile(fileId);
        clientService.save(clientDoc);
        re.addAttribute("id", clientDoc.getId());
        return RouteUtils.redirectTo(ClientAdminRoutes.ALL);
    }


    @RequestMapping(value = ClientAdminRoutes.ALL, method = RequestMethod.GET)
    public String all(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                      Model model) {
        SearchClientAdminRequest searchClientAdminRequest = new SearchClientAdminRequest(currentPage);
        SearchClientAdminResponse searchClientAdminResponse = clientService.findAll(searchClientAdminRequest);
        model.addAttribute("searchClientAdminResponse", searchClientAdminResponse);
        return "com.ub.clientTest.admin.all";
    }

    @RequestMapping(value = ClientAdminRoutes.DELETE, method = RequestMethod.GET)
    public String delete(@RequestParam ObjectId id, Model model) {
        model.addAttribute("id", id);
        return "com.ub.clientTest.admin.delete";
    }

    @RequestMapping(value = ClientAdminRoutes.DELETE, method = RequestMethod.POST)
    public String delete(@RequestParam ObjectId id) {
        clientService.delete(id);
        return RouteUtils.redirectTo(ClientAdminRoutes.ALL);
    }
}
