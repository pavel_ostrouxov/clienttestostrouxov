package com.ub.clientTest.client.modules;

import com.ub.client.clientType.models.ClientTypeDoc;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Document
public class ClientDoc {

    private ObjectId id;
    private String email;
    private String name;
    private String lastName;
    private ObjectId pic;
    private ObjectId file;
    private ClientTypeDoc clientTypeDoc;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ObjectId getPic() {
        return pic;
    }

    public void setPic(ObjectId pic) {
        this.pic = pic;
    }

    public ObjectId getFile() {
        return file;
    }

    public void setFile(ObjectId file) {
        this.file = file;
    }

    public ClientTypeDoc getClientTypeDoc() {
        return clientTypeDoc;
    }

    public void setClientTypeDoc(ClientTypeDoc clientTypeDoc) {
        this.clientTypeDoc = clientTypeDoc;
    }
}
